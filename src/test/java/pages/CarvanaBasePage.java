package pages;

import org.junit.Test;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.Driver;

import java.util.List;

public class CarvanaBasePage {

    public CarvanaBasePage(){
        PageFactory.initElements(Driver.getDriver(), this);
    }

    // Common elements from header, footer and other common elements

    @FindBy(css = "div.Logo__LogoWrapper-sc-14r2405-0.fSZhVx")
    public WebElement logo;

    @FindBy(xpath = "//div[@data-qa='menu-wrapper']")
    public List<WebElement> menuItems;

    @FindBy(xpath = "//a[@data-cv-test='headerSignInLink']")
    public WebElement signInButton;

    @FindBy(id = "email")
    public WebElement emailInput;

    @FindBy(css = "button[type=submit]")
    public WebElement continueButton;

    @FindBy(id = "password")
    public WebElement passwordInput;

    @FindBy(id = "error-banner")
    public WebElement incorrectEmailOrPassword;

    @FindBy(xpath = "//a[@data-cv-test='headerSearchLink']")
    public WebElement searchCarsButton;

    @FindBy(css = "div[data-qa='menu-flex']")
    public WebElement searchMenuFilters;

    @FindBy(css = "input[data-test='SearchBarInput']")
    public WebElement searchInput;

    @FindBy(css = "button[data-qa='go-button']")
    public WebElement goButton;

    @FindBy(css = "div[class='result-tile']")
    public List<WebElement> resultTiles;

    @FindBy(css = "picture[class='vehicle-image']")
    public WebElement vehicleImage;

    @FindBy(css = "div[data-qa='base-favorite-vehicle']")
    public WebElement favoriteButton;

    @FindBy(css = "div[class='tk-frame middle-frame']")
    public List<WebElement> tileBody;

    @FindBy(css = "div[data-qa='base-inventory-type']")
    public WebElement inventoryType;

    @FindBy(css = "div[class='year-make']")
    public WebElement yearMake;

    @FindBy(css = "div[class='trim-mileage']")
    public WebElement trimMileage;

    @FindBy(css = "div[data-qa='price']")
    public WebElement price;

    @FindBy(css = "div[data-qa='monthly-payment']")
    public WebElement monthlyPayment;

    @FindBy(css = "div[class='down-payment']")
    public WebElement downPayment;

    @FindBy(css = "div[class*='top-[-1px]']")
    public WebElement deliveryChip;
}
