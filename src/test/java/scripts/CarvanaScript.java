package scripts;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.CarvanaSearchCarsPage;
import utils.Waiter;

public class CarvanaScript extends CarvanaBaseScript {

    @BeforeMethod
    public void setPage(){
    }

    @Test(priority = 1, testName = "Validate Carvana home page title and url")
    public void validateHomePageTitleandURL(){
        Assert.assertEquals(driver.getTitle(), "Carvana | Buy & Finance Used Cars Online | At Home Delivery");
        Assert.assertEquals(driver.getCurrentUrl(), "https://www.carvana.com/");
    }

    @Test(priority = 2, testName = "Validate the Carvana logo")
    public void validateCarvanaLogo(){
        Assert.assertTrue(carvanaBasePage.logo.isDisplayed());
    }

    @Test(priority = 3, testName = "Validate the main navigation section items")
    public void validateMainNavigationSectionItems(){
        for(WebElement webElement : carvanaBasePage.menuItems){
            Assert.assertTrue(webElement.isDisplayed());
        }
    }

    @Test(priority = 4, testName = "Validate the sign in error me")
    public void validateSignInErrorMessage(){
        Waiter.waitUntilClickable(carvanaBasePage.signInButton, 5);
        carvanaBasePage.signInButton.click();
        carvanaBasePage.emailInput.sendKeys("johndoe@gmail.com");
        carvanaBasePage.continueButton.click();
        carvanaBasePage.passwordInput.sendKeys("abcd1234");
        carvanaBasePage.continueButton.click();

        Assert.assertEquals(carvanaBasePage.incorrectEmailOrPassword.getText(), "Email address and/or password combination is incorrect.");
    }

    @Test(priority = 5, testName = "Validate the search filter options and search button")
    public void validateSearchFilterOptions(){
        Waiter.waitUntilClickable(carvanaBasePage.searchCarsButton, 5);
        carvanaBasePage.searchCarsButton.click();
        Assert.assertEquals(driver.getCurrentUrl(), "https://www.carvana.com/cars");

        Assert.assertEquals(carvanaBasePage.searchMenuFilters.getText(),
                "PAYMENT & PRICE\n" +
                "MAKE & MODEL\n" +
                "BODY TYPE\n" +
                "YEAR & MILEAGE\n" +
                "FEATURES\n" +
                "MORE FILTERS");
    }

    @Test(priority = 6, testName = "Validate the search result tiles")
    public void validateSearchResultTiles(){
        Waiter.waitUntilClickable(carvanaBasePage.searchCarsButton, 5);
        carvanaBasePage.searchCarsButton.click();
        carvanaBasePage.searchInput.sendKeys("mercedes-benz");
        carvanaBasePage.goButton.click();
        Waiter.pause(2);
        Assert.assertTrue(driver.getCurrentUrl().contains("mercedes-benz"));

        for (int i = 0; i < carvanaBasePage.resultTiles.size(); i++) {
            Assert.assertTrue(carvanaBasePage.vehicleImage.isDisplayed());
            Assert.assertTrue(carvanaBasePage.favoriteButton.isEnabled());
            Assert.assertTrue(carvanaBasePage.tileBody.get(i).isDisplayed());

            for (int j = 0; j < carvanaBasePage.tileBody.size(); j++) {
                Assert.assertTrue(carvanaBasePage.inventoryType.isDisplayed());
                Assert.assertTrue(carvanaBasePage.yearMake.isDisplayed());
                Assert.assertTrue(carvanaBasePage.trimMileage.isDisplayed());
                Assert.assertTrue(carvanaBasePage.price.isDisplayed());
                Assert.assertTrue(carvanaBasePage.monthlyPayment.isDisplayed());
                Assert.assertTrue(carvanaBasePage.downPayment.isDisplayed());
                Assert.assertTrue(carvanaBasePage.deliveryChip.isDisplayed());
            }
        }
    }


}
